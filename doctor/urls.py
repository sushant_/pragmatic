from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

app_name = 'doctor'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logging_in/$', views.logging_in, name='logging_in'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^details/(?P<patient_id>[0-9]+)/$', views.details, name='details'),
    url(r'^add_record/(?P<patient_id>[0-9]+)/$', views.add_record, name='add_record'),
    url(r'^add_patient/$', views.add_patient, name='add_patient'),
    url(r'^adding_patient/$', views.adding_patient, name='adding_patient'),
    url(r'^adding/(?P<patient_id>[0-9]+)/$', views.adding, name='adding'),
    url(r'^dosage_monitor/(?P<patient_id>[0-9]+)/$', views.dosage_monitor, name='dosage_monitor'),
    url(r'^view_record/$', views.view_record, name='view_record'),
    url(r'^view_form/$', views.view_form, name='view_form'),
    url(r'^viewing_record/$', views.viewing_record, name='viewing_record'),
    url(r'^logout/$', views.logout, name='logout'),
    #url(r'^overview/$', views.patient_dashboard, name='patient_dashboard'),
    #url(r'^medical_record/$', views.patient_view_record, name='patient_view_record'),
    #url(r'^medicine_feedback/$', views.medicine_feedback, name='medicin_feedback'),
    #url(r'^adding_feedback/$', views.adding_feedback, name='adding_feedback'),
    #url(r'^sw(.*.js)$', views.service_worker, name='service_worker'),
    #url(r'^medicine_details/$', views.medicine_details, name='medicine_details'),
    #url(r'^log_in/$', views.pat_login, name='pat_login'),
    #url(r'^logging/$', views.pat_logging, name='pat_logging'),
    #url(r'^log_out/$', views.log_out, name='log_out'),
    #url(r'^register/$', views.register, name='register'),
    #url(r'^registering/$', views.registering, name='registering'),
    #url(r'^$', views.index, name='index'),
    #url(r'^login/$', views.login, name='login'),
    
]