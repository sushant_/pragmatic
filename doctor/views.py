from django.shortcuts import render

# Create your views here.
from .models import *
from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django import template
from django.template.loader import get_template 

def index(request):
	return render(request, 'doctor/index.html')

# Login html page
def login(request):
	return render(request, 'doctor/login.html')

# Backend work to login
def logging_in(request):
	
	try:
		login_doctor = Doctor.objects.get(username = request.POST['doctor_username'])

	except(KeyError, Doctor.DoesNotExist):
       		 # Redisplay the question voting form.
	    	return render(request, 'doctor/login.html', {
           	 #'question': question,
        	'error_message': "Incorrect username.",
        	})

	else:
		if login_doctor.password == request.POST['doctor_password'] :
			request.session['doctor_id'] = login_doctor.id
			return HttpResponseRedirect(reverse('doctor:dashboard'))
		else :
			return render(request, 'doctor/log_in.html', {
                	'error_message': "Invalid password.",
        		})


# Show overview of patient
def dashboard(request):
	
	login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])
	mp = {}
	p_list = []
	for r in reversed(login_doctor.record_set.all()):
		if r.patient.id not in mp :
			p_list.append(r.patient)
			mp[r.patient.id] = 1
	return render(request, 'doctor/dash.html',
		{
			'doctor': login_doctor,
			'patient_list' : p_list,
		})			
	
	'''meds_list = []

	for record in reversed(login_doctor.record_set.all()):
		ar = []

		for med in record.medicine_set.all():
			ar.append(med.name)
		meds_list.append(ar)	
			
	return render(request, 'doctor/record.html',
		{
			'doctor': login_doctor,
			'record_list' : reversed(login_doctor.record_set.all()),
			'meds_list' : meds_list
		})
	'''
# Show the respective patient details
def details(request, patient_id):

	login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])
	check_patient = Patient.objects.get(pk = patient_id)
	meds_list = []
	records_list = []

	for record in reversed(check_patient.record_set.all()):
		if record.doctor.id == login_doctor.id:
			records_list.append(record)
			ar = []

			for med in record.medicine_set.all():
				ar.append(med.name)
			meds_list.append(ar)	
			
	return render(request, 'doctor/details.html',
		{
			'doctor': login_doctor,
			'patient': check_patient,
			'record_list' : records_list,
			'meds_list' : meds_list
		})

	
# Show past records
def view_form(request):
	return render(request, 'doctor/view_form.html')

def viewing_record(request):

	try:
		login_patient = Patient.objects.get(username = request.POST['username'])

	except(KeyError, Patient.DoesNotExist):
       		 # Redisplay the question voting form.
	    	return render(request, 'doctor/view_form.html', {
           	 #'question': question,
        	'error_message': "Incorrect username.",
        	})

	else:
		if login_patient.password == request.POST['pt_password'] :
			request.session['patient_id'] = login_patient.id
			print "here"
			return view_record(request)
		else :
			return render(request, 'doctor/view_form.html', {
                	'error_message': "Invalid password.",
        		})


def view_record(request):

	login_patient = Patient.objects.get(pk=request.session['patient_id'])
	login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])
	
	meds = []
	for r in login_patient.record_set.all():
		ar = []
		for m in r.medicine_set.all():
			ar.append(m.name)
		meds.append(ar)
		#print meds[0]
		#print meds[0]


	return render(request, 'doctor/record.html',
		{
			'doctor': login_doctor,
			'patient' : login_patient,
			'record_list' : login_patient.record_set.all(),
			'meds': meds
		})

# Return ADD page html
def add_record(request, patient_id):

	login_patient = Patient.objects.get(pk=patient_id)
	login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])

	no_of_meds = 0
	if 'no_of_meds' in request.session:
		no_of_meds = request.session['no_of_meds']
	return render(request, 'doctor/add.html',
		{
			'doctor': login_doctor,
			'patient' : login_patient,
			'no_of_meds': no_of_meds,
		})

# Adding backend part
def adding(request, patient_id):

	login_patient = Patient.objects.get(pk=patient_id)
	login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])
	
	if 'no_of_meds' not in request.session:
		request.session['symptoms'] = request.POST['symptoms']
		request.session['disease'] = request.POST['disease']
		request.session['no_of_meds'] = request.POST['no_med']

		return add_record(request, patient_id)

	else:		
		r = Record(patient = login_patient, doctor = login_doctor, 
			date_of_visit = datetime.now().date(), 
		symptoms = request.session['symptoms'], disease = request.session['disease'])
		r.save()		
		n = int(request.session['no_of_meds'])
		for i in range(n):
			name = request.POST['name' + str(i+1)]
			dt = datetime.now().date()
			end = request.POST['end' + str(i+1)]
			t = request.POST['times' + str(i+1)]
			m = Medicine(name = name, start_date = dt, end_date = end, no_of_times = t)
			m.save()
			m.record.add(r)

		del request.session['symptoms']
		del request.session['disease']
		del request.session['no_of_meds']

		return dashboard(request)	

def add_patient(request):
	return render(request, 'doctor/add_patient.html')

def adding_patient(request):

	try:
		check_patient = Patient.objects.get(username = request.POST['username'])

	except(KeyError, Patient.DoesNotExist):
       		 # Redisplay the question voting form.
	    	return render(request, 'doctor/add_patient.html', {
           	 #'question': question,
        	'error_message': "Incorrect username.",
        	})

	else:
		if check_patient.mobile_no == request.POST['number'] :
			return add_record(request, check_patient.id)
		else :
			return render(request, 'doctor/add_patient.html', {
                	'error_message': "Invalid password.",
        		})



import json

def dosage_monitor(request, patient_id):

	login_patient = Patient.objects.get(pk=patient_id)
	login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])
	
	records = login_doctor.record_set.all()
	dates = {}
	val = {}
	meds = []
	arr = []
	temp_dates = []

	for record in records:
		if record.patient.id == login_patient.id:
			for medicine in record.medicine_set.all():
				for dose in medicine.dosage_set.all():
					if dose.patient.id == login_patient.id:
						arr.append(dose.no_of_times_taken)
						temp = []
						temp.append(dose.consuming_date.date().year)
						temp.append(dose.consuming_date.date().month)
						temp.append(dose.consuming_date.date().day)
						temp_dates.append(temp)
				
				meds.append(medicine.name)
				dates[medicine.name] = []
				for v in temp_dates:
					dates[medicine.name].append(v);		
				val[medicine.name] = []
				for v in arr:
					val[medicine.name].append(v);		
				
				#dos_monitor[medicine.name]=[]
				#for v in arr:
				#	dos_monitor[medicine.name].append(v)
				del arr[:]
				del temp_dates[:]			

	return render(request, 'doctor/dosage_monitor.html',
		{
			'doctor': login_doctor,
			'patient' : login_patient,
			'meds' : json.dumps(meds),
			'meds2': meds,
			'dates' : json.dumps(dates),
			'val':json.dumps(val),
		})					

def logout(request):
	try:
        	del request.session['doctor_id']
        	#del request.session['patient_id']
    	except KeyError:
        	pass
    	return index(request)
