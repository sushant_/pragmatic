from __future__ import unicode_literals

from django.db import models
from datetime import  datetime

# Create your models here.
class Doctor(models.Model):
	name = models.CharField(max_length=200)
	username = models.CharField(max_length=200)
	education = models.CharField(max_length=200)
	speciality = models.CharField(max_length=200)
	password = models.CharField(max_length=200)

	def __str__(self):
        	return self.name

class Patient(models.Model):
	name = models.CharField(max_length=200)
	username = models.CharField(max_length=200)
	mobile_no = models.CharField(max_length=15)
	password = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	dob = models.DateField()
	address = models.CharField(max_length=200)

	def __str__(self):
        	return self.name

class Record(models.Model):
	patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
	doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
	date_of_visit = models.DateField()
	symptoms = models.CharField(max_length=200)
	disease = models.CharField(max_length=200)

	def __str__(self):
        	return "%s %s" % (self.patient.name, self.doctor.name)

class Medicine(models.Model):
	name = models.CharField(max_length=200)
	record = models.ManyToManyField(Record)
	start_date = models.DateTimeField(default=datetime.now, blank=True)
	end_date = models.DateField()
	no_of_times = models.IntegerField(default=0)
	
	def __str__(self):
        	return self.name

class Dosage(models.Model):
	medicine = models.ForeignKey(Medicine, on_delete=models.CASCADE)
	patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
	consuming_date = models.DateTimeField(default=datetime.now, blank=True)
	#end_date = models.DateField()
	#no_of_times = models.IntegerField(default=0)
	no_of_times_taken = models.IntegerField(default=0)

	def __str__(self):
        	return "%s %s" % (self.patient.name, self.medicine.name)		

'''class Monitor(models.Model):
	medicine = models.ForeignKey(Medicine, on_delete=models.CASCADE)
	patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
	 
	start_date = models.DateTimeField(default=datetime.now, blank=True)

	def __str__(self):
        	return "%s %s" % (self.patient.name, self.medicine.name)		
'''