from django.shortcuts import render

# Create your views here.
from doctor.models import *
from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django import template
from django.template.loader import get_template 

def index(request):
	print "here"
	return render(request, 'patient/index.html')

def login(request):
	return render(request, 'patient/login.html')

def logging(request):
	
	try:
		login_patient = Patient.objects.get(username= request.POST['username'])

	except(KeyError, Patient.DoesNotExist):
       		 # Redisplay the question voting form.
	    	return render(request, 'patient/login.html', {
           	 #'question': question,
        	'error_message': "You didn't select a choice.",
        	})

	else:
		if login_patient.password == request.POST['passwd'] :
			request.session['patient_id'] = login_patient.id
			return HttpResponseRedirect(reverse('patient:dashboard'))
		else :
			return render(request, 'patient/login.html', {
                	'error_message': "Invalid password.",
        		})

def medicine_rem():

	login_patient = Patient.objects.get(pk=1)
	meds = []

	for record in login_patient.record_set.all():
		for medicine in record.medicine_set.all():
			if medicine.end_date >= datetime.now().date():
				
				hr = datetime.now().hour
				#if dose.no_of_times_taken < dose.no_of_times:
					
				#meds.append(medicne.name)
				if medicine.no_of_times == 1 and hr >=6 and hr < 12:
					meds.append(medicine.name)

				elif medicine.no_of_times == 2:
					if hr >= 6 and hr < 12:
						meds.append(medicine.name)
					elif hr >= 20 and hr < 24:
						meds.append(medicine.name)	
									
				elif medicine.no_of_times == 3:
					if hr >= 6 and hr < 12:
						meds.append(medicine.name)
					elif hr >= 14 and hr < 16:
						meds.append(medicine.name)
					elif hr >= 18 and hr < 24:
						meds.append(medicine.name)
	return meds					

def dashboard(request):
	
	login_patient = Patient.objects.get(pk=request.session['patient_id'])
	#login_doctor = Doctor.objects.get(pk=request.session['doctor_id'])
	
	meds_to_eat = []
	meds_to_eat = medicine_rem()
	#login_patient = Patient.objects.get(pk=1)

	return render(request, 'patient/dashboard.html',
		{
			'patient' : login_patient,
			'meds_to_eat' : meds_to_eat,
			'f': len(meds_to_eat),

		})

def view_record(request):
	
	login_patient = Patient.objects.get(pk=request.session['patient_id'])
	
	meds = []
	for r in login_patient.record_set.all():
		ar = []
		for m in r.medicine_set.all():
			ar.append(m.name)
		meds.append(ar)
		#print meds[0]
		#print meds[0]

	return render(request, 'patient/record.html',
		{
			'patient' : login_patient,
			'record_list' : login_patient.record_set.all(),
			'meds' : meds,
		})

import requests
class med_details:
	name=""
	price=0
	size=""

def medicine_details(request):
	
	login_patient = Patient.objects.get(pk=request.session['patient_id'])
	#login_patient = Patient.objects.get(pk=1)
	name = []
	med_detls = {}
	f=0

	if 'med_name' not in request.POST:
		f=0	
	else:
		med_name = request.POST['med_name']	
		url =  "http://www.healthos.co/api/v1/search/medicines/brands/" + str(med_name)
		re = requests.get(url, headers = {
			'authorization': 'Bearer c23d02df2beb3ecbe01e10999847387454adf00d3a0c4e7cbec7bfb3ca2162a8'
			})
		if re.status_code != 200 :
			f= re.status_code	
		else:
			f=2
			d = re.json()
			for i in d:
				name.append(i['name'])
				med_detls[i['name']] = []
				med_detls[i['name']].append(i['price'])
				med_detls[i['name']].append(i['size'])
	
	return render(request, 'patient/medicine_details.html',
		{
			'patient' : login_patient,
			'f':f,
			'name' :name,
			'med_detls' : med_detls,
		})	

def medicine_feedback(request):
	
	login_patient = Patient.objects.get(pk=request.session['patient_id'])
	#login_patient = Patient.objects.get(pk=1)
	meds = {}

	for record in login_patient.record_set.all():
		for med in record.medicine_set.all():
			if med.end_date >= datetime.now().date():
				meds[med] = med.no_of_times

	return render(request, 'patient/medicine_reminder.html',
		{
			'patient' : login_patient,
			'med_list': meds,
		})		

def adding_feedback(request):

	login_patient = Patient.objects.get(pk=request.session['patient_id'])
	#login_patient = Patient.objects.get(pk=1)
	date = datetime.now()

	for key in request.POST:
		if key.startswith('med_'):
			cnt=0
			for t in request.POST.getlist(key):
				cnt+=1
			med = Medicine.objects.get(pk= key[4:])
			d = Dosage(medicine = med, patient = login_patient,
				consuming_date = date, no_of_times_taken = cnt)
			d.save()

	return dashboard(request)				 			

def register(request):
	return render(request, 'patient/register.html')

def registering(request):
	try:
		pat = Patient.objects.get(email = request.POST['email'])	

	except(KeyError, Patient.DoesNotExist):
		p = Patient(email = request.POST['email'] ,
			name = request.POST['name'],
			password = request.POST['passwd'],
			dob = request.POST['dob'],
			address = request.POST['addr'],
			mobile_no = request.POST['mobileno'],
			username = request.POST['username'])

		p.save()		

		return render(request, 'patient/login.html')
	else:
		return render(request, 'patient/register.html')
	
def doctor_list(request):
	return render(request, 'patient/doctor_list.html',
		{
			'doc_list' : Doctor.obejcts.all(),
		})

def logout(request):
	try:
        	del request.session['patient_id']
    	except KeyError:
        	pass
    	return index(request)


