from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

app_name = 'patient'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logging/$', views.logging, name='logging'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^view_record/$', views.view_record, name='view_record'),
    url(r'^medicine_details/$', views.medicine_details, name='medicine_details'),
    url(r'^medicine_feedback/$', views.medicine_feedback, name='medicine_feedback'),
    url(r'^adding_feedback/$', views.adding_feedback, name='adding_feedback'),
    url(r'^register/$', views.register, name='register'),
    url(r'^registering/$', views.registering, name='registering'),
    url(r'^logout/$', views.logout, name='logout'),
    
]    
